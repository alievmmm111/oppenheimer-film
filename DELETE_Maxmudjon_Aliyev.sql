BEGIN;
-- Remove the film from the inventory
DELETE FROM inventory WHERE film_id = (SELECT film_id FROM film WHERE title = 'Oppenheimer');

-- Remove all corresponding rental records
DELETE FROM rental WHERE inventory_id NOT IN (SELECT inventory_id FROM inventory);

-- Remove any records related to you (as a customer) from all tables except "Customer" and "Inventory"
DECLARE customer_id INT;
SET customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Maxmudjon' AND last_name = 'Aliyev');

DELETE FROM payment WHERE customer_id = customer_id;
DELETE FROM rental WHERE customer_id = customer_id;
COMMIT;