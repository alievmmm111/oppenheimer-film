DO $$
DECLARE
    film_id INT;
    actor_ids INT[];
BEGIN
    -- 1) Insert the film into the film table and save its ID
    INSERT INTO film (
        title, 
        description, 
        release_year, 
        rental_duration, 
        rental_rate, 
        length
    )
    VALUES (
        'Oppenheimer', 
        'Oppenheimer is a biographical thriller film that dramatizes the life story of J. Robert Oppenheimer, 
        the physicist who played a significant role in the development of the atomic bomb', 
        2023, 
        14, 
        4.99, 
        180
    )
    RETURNING film_id INTO film_id;

    -- 2) Insert actors into the actor table and save their IDs
    INSERT INTO actor (first_name, last_name)
    VALUES
        ('Cillian', 'Murphy'),
        ('Emily', 'Blunt'),
        ('Matt', 'Damon')
    RETURNING actor_id
    INTO actor_ids;

    -- 3) Associate actors with the film in the film_actor table
    INSERT INTO film_actor (film_id, actor_id)
    SELECT film_id, actor_id
    FROM unnest(actor_ids) actor_id;

    -- 4) Add the film to the store's inventory (store_id = 1)
    INSERT INTO inventory (film_id, store_id)
    VALUES (film_id, 1);
END $$;
