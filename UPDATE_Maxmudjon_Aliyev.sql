BEGIN;
DECLARE film_id INT;
SET film_id = (SELECT film_id FROM film WHERE title = 'Oppenheimer');
UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE film_id = film_id;

-- Customer with at least 10 rental and 10 payment records
DECLARE customer_id INT;
SET customer_id = (
    SELECT customer_id 
    FROM customer 
    WHERE (
        SELECT COUNT(*) FROM rental WHERE customer_id = customer.customer_id
    ) >= 10 AND (
        SELECT COUNT(*) FROM payment WHERE customer_id = customer.customer_id
    ) >= 10
    LIMIT 1
);

-- Update customer data
UPDATE customer
SET first_name = 'Maxmudjon', last_name = 'Aliyev', address_id = (SELECT address_id FROM address LIMIT 1), create_date = CURRENT_DATE
WHERE customer_id = customer_id;
COMMIT;